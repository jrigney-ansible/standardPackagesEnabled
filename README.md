# standardPackagesEnabled

Install a set of standard packages

See vars/main.yml for a list of the packages installed

## Dependency
role: ansibleEnabled


## Sample Playbook
~~~
---
- hosts: <host>
  remote_user: ansible
  roles:
    - standardPackagesEnabled
~~~

## Sample command line
~~~
ansible-playbook --inventory ./hosts playbook/standardPackagesEnabled.yml
~~~

